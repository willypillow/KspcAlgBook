#!/bin/bash

FILES=" \
	contents/basic-cpp.md \
	contents/competition-basics.md \
	contents/dynamic-programming-basics.md \
"

TYPE="markdown_github+yaml_metadata_block+tex_math_dollars+footnotes-ascii_identifiers"

FLAGS=" \
	--standalone \
	--table-of-contents \
	--number-sections \
	--smart \
	--tab-stop=2 \
"

HTMLFLAGS="--katex --css=style.css"

# Generate a HTML for each chapter
touch tmp.md
rm -r public/
mkdir public
i=0
for f in $FILES; do
	ff=$(basename "${f%%.*}").html
	pandoc $HTMLFLAGS $FLAGS --number-offset=$i -f $TYPE -t html --template=toc-template $f | sed 's/<a href="\(.*\)"/<a href="'$ff'\1"/' >> tmp.md
	pandoc $HTMLFLAGS $FLAGS --number-offset=$i -f $TYPE -t html -o public/$ff _goback.md $f
	i=$((i + 1))
done
pandoc $HTMLFLAGS --standalone -f $TYPE -o public/index.html metadata.md tmp.md
rm tmp.html
cp style.css public/

## Dirty trick to convert Gitlab style math to pandoc style
#echo "" > tmp.md
#for f in $FILES; do
#	cat $f | tr '\n' '\a' | sed -e 's/\$`/$/g' -e 's/`\$/$/g' -e 's/```[ ]*math\([^`]*\)```/$$\1$$/g' | tr '\a' '\n' >> tmp.md
#done

## Generate single HTML
## The generated file does NOT work offline due to Katex dependencies
#pandoc $FLAGS $HTMLFLAGS \
#	-f $TYPE -t html -o output/out.html metadata.md $FILES
#cp style.css output/

## Generate PDF
#pandoc $FLAGS \
#	--latex-engine=xelatex \
#	--template=eisvogel \
#	--variable=titlepage:true \
#	--variable=fontsize:12pt \
#	--variable=links-as-notes:true \
#	--variable=CJKmainfont:'Noto Sans CJK TC' \
#	-f $TYPE -o out.pdf metadata.md $FILES
