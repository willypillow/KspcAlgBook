# 競賽基礎

## Online Judge 雜談

### 接收輸入

在 Online Judge （線上評測系統，簡稱 OJ ，如 [ZeroJudge](https://zerojudge.tw/) ）上寫題目時，常會有類似「輸入的每一行有一個整數 $n$」，但沒有告訴你有幾行的情形。此時可以用類似以下的方法：

``` cpp
int n;
while (cin >> n) {
	// 處理一個 n
}
```

### 評測結果

送出程式後， OJ 通常會回傳評測的結果，其代表的意思簡述如下：

簡寫 | 意涵
-----|---------------------
AC   | Accepted （答案正確）
WA   | Wrong Answer （答案錯誤）
TLE  | Time Limit Exceeded （超過時限）
MLE  | Memory Limit Exceeded （超過記憶體限制）
PE   | Presentation Error （輸出格式錯誤）
RE   | Runtime Error （執行時期錯誤，可能是 MLE 、陣列讀寫出界、數字除以 0 ...等）
CE   | Compile Error （編譯錯誤）

要注意的是，一個 OJ 不一定有（也不僅限於）以上所有的訊息，例如很多會把 PE 直接判成 WA 。詳情請閱讀該 OJ 的說明文件。

## 複雜度分析

### Big-Oh 表示法

在競賽中，估計演算法的時間、空間效率，以避免 TLE/MLE 是極為重要的。為了達到這個目的，我們通常會使用 big-oh 這個工具。

通俗地說， $f(n) = O(g(n))$ 表示 $f(n)$ 成長速率不會超過 $g(n)$。例如，若令 $f(n) = 4n^2 + 2n + 3$ ，則會發現在 $n$ 很大的時候， $n^2$ 對於 $f(n)$ 的大小有主導的地位。因此，這時 $f(n) = O(n^2)$ 。

嚴謹地說， $f(n) = O(g(n))$ 成立若且唯若存在一個常數 $M > 0$ 使得對於夠大的數字 $n$， $|f(n)| < M|g(n)|$。換言之，$f(n) = O(g(n)) \leftrightarrow \exists M \in R \land M > 0, x_0 \in R \rightarrow |f(x)| < M|g(n)| \forall x \ge x_0$

舉個簡單的例子，對於以下的程式碼

``` cpp
for (int i = 0; i < n; i++) {
	for (int j = 0; j < i; j++) ans += i + j;
}
```

由於會執行約 $n(n - 1) / 2$ 次運算，時間複雜度便是 $O(n^2)$ 。

那麼要怎麼將 big-oh 應用在題目上？一般來說，若執行時間 $t(n) = O(g(n))$ ，可以直接將 $n$ 代入 $g(n)$ ，得到粗略的結果。如果 $g(n) < 10^8 \sim 10^9$ ，那只要「常數」（constant factor，也就是第一個例子中的 $4$ 、 $2n + 3$）不要太大，那麼就很有機會在 1 秒內跑完。

以下是在 1 秒內，常見的複雜度和 $n$ 的大略關係。（和常數與主機速度有關，僅供參考）

複雜度       | $n$
-------------|-----------------
$O(n)$     | $10^8 \sim 10^9$
$O(nlogn)$ | $10^6$
$O(n^2)$   | $10^4$
$O(n^3)$   | $500$
$O(n^4)$   | $100$
$O(2^n)$   | $2^{20}$
$O(n!)$    | $11$

#### 延伸閱讀
[維基百科：一些相關的漸進符號](https://zh.wikipedia.org/wiki/%E5%A4%A7O%E7%AC%A6%E5%8F%B7#.E4.B8.80.E4.BA.9B.E7.9B.B8.E5.85.B3.E7.9A.84.E6.B8.90.E8.BF.91.E7.AC.A6.E5.8F.B7)
