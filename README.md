# KSPC 程式競賽講義

[目錄](https://willypillow.gitlab.io/KshsAlgBook/)

### 下載

離線版的 PDF 可在 <https://gitlab.com/WillyPillow/KspcAlgBook/builds/artifacts/master/raw/out.pdf?job=pages> 下載。

### 手動編譯

你需要一個 Linux 系統，並安裝以下 packages （適用於 Arch Linux ，其它 distro 使用者請自行搜尋等價的 packages）：

- `git`
- `pandoc`
- `texlive-core`
- `texlive-latexextra`
- `ttf-dejavu`
- `noto-fonts-cjk`

執行以下指令：

```
git clone https://gitlab.com/WillyPillow/KspcAlgBook
cd KspcAlgBook
./build.sh
```

檔案即會生成到 `out.pdf` 。

## Contributing

本 project 是個 work in progress ，歡迎各位的 contribution。

### Style Guide

1. 本 project 使用 [Gitlab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)
1. 請統一使用井字號式標題
1. 為了 Windows 支援性，請使用 dos 式換行（ `\r\n` ）
1. 請使用 tab 縮排
1. 請在全形和半形文字間加上一個空格
1. 若要新增一個章節（ h1 標題），請在 [contents](contents/) 資料夾中新增一個 `.md` 檔，檔名使用簡短的英數字，單字間用 `-` 分隔，並在 [build.sh](build.sh) 的 `FILES` 變數插入該章節。除此之外，檔案開頭必須有如下的 YAML Metadata：
	```
	---
	title: [該章標題]
	---
	```
1. 引用題目請使用以下的格式：
	```
	> [OJ 簡稱] [題號]
	>
	> [原題連結]
	>
	> [題目簡述]
	```

### 步驟

1. Fork 此 repo （<https://docs.gitlab.com/ce/gitlab-basics/fork-project.html>）
1. 新增一個 branch
1. 作出更改 / commits
1. 送出 Merge Request （<https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html>）

若有任何問題，歡迎在 [Issue Tracker](issues/) 上發問。

## License

本 project 採用 CC0 授權，詳細請見 [LICENSE](LICENSE) 。

## Credits

PDF Template 使用 [Eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template) ， BSD 3-Clause 授權。

HTML CSS 使用 <https://gist.github.com/killercup/5917178> ， Public Domain 。

Note: 由於主要 commit 自本人的筆電，本 repo 並未經過 GPG 簽名，請謹慎服用 :P

Note: Due to the fact that most commits are from my laptop, this repo is unsigned. Please use with care :P
