FROM debian:stretch-slim

MAINTAINER James Gregory <james@jagregory.com>

# install latex packages
RUN apt-get update -y \
  && apt-get install -y -o Acquire::Retries=10 --no-install-recommends \
    pandoc \
    texlive-latex-recommended \
    texlive-xetex \
    texlive-luatex \
    texlive-science \
    fontconfig \
    lmodern \
    fonts-noto-cjk
